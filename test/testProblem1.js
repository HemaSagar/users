const users = require('../users.js')
const problem1 = require('../problem1.js')

const result = problem1(users);

console.log(`${result.first_name} ${result.last_name} is a ${result.gender} and can be contacted on ${result.email}`);

