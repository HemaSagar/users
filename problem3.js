function problem3(users) {
    users.sort((a, b) => {
        const x = a.last_name.toLowerCase();
        const y = b.last_name.toLowerCase();
        return x < y ? -1 : x > y ? 1 : 0;
    })

    return users;
}

module.exports = problem3;
