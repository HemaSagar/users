function problem4(users) {
    const emails = [];
    if (users instanceof Array) {
        for (let i = 0; i < users.length; i++) {
            emails.push(users[i].email);
        }
    return emails;
    }

    return [];
    
}

module.exports = problem4;