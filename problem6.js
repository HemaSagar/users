function problem6(users) {
    const genderFiltered = [[], [], [], [], [], [], []];

    for (let i = 0; i < users.length; i++) {
        const currUser = users[i];
        const currGender = currUser.gender.toLowerCase();
        const genderObj = { male: 0, female: 1, polygender: 2, bigender: 3, genderqueer: 4, genderfluid: 5, agender: 6 };

        switch (genderObj[currGender]) {
            case 0:
                genderFiltered[0].push(currUser);
                break;

            case 1:
                genderFiltered[1].push(currUser);
                break;

            case 2:
                genderFiltered[2].push(currUser);
                break;

            case 3:
                genderFiltered[3].push(currUser);
                break;

            case 4:
                genderFiltered[4].push(currUser);
                break;

            case 5:
                genderFiltered[5].push(currUser);
                break;

            case 6:
                genderFiltered[6].push(currUser);
                break;


        }

    }
    return genderFiltered;

}


module.exports = problem6;