function problem5(users) {
    const maleEmails = [];

    for (let i = 0; i < users.length; i++) {
        let currObj = users[i];

        if (currObj.gender.toLowerCase() === 'male') {
            maleEmails.push([`${currObj.first_name} ${currObj.last_name}`, currObj.email])
        }
    }
    return maleEmails;
}


module.exports = problem5;