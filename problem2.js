function problem2(users) {
    const index = users.length-1;
    const user = users[index];

    const result = `Last user is ${user.first_name} ${user.last_name} and can be contacted on ${user.email}`;
    
    console.log(result);
}

module.exports = problem2;